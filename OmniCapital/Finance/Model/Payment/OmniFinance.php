<?php
namespace OmniCapital\Finance\Model\Payment;
use Magento\Payment\Model\Method\AbstractMethod;

class OmniFinance extends \Magento\Payment\Model\Method\AbstractMethod
{
	
	const METHOD_CODE   = 'omni_finance';
	protected $_code	= self::METHOD_CODE;
	
	protected $_isGateway               = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = true;
    protected $_canRefund               = FALSE;
	
	protected $_minOrderTotal 			= 0;
    protected $_isOffline 				= true;
	
	protected $_minAmount;
	protected $_maxAmount;
	
	protected $_supportedCurrencyCodes = array('USD','GBP','EUR');
	
	protected $_liveUrl	= 'https://omniport.omnicapital.co.uk/credit_app/Default.aspx';
    protected $_testUrl = 'https://omniporttest.ocrf.co.uk/credit_app/Default.aspx';
	
	public function __construDFct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $resource,
            $resourceCollection,
            $data
        );
        
		$this->_code = 'omni_finance';
        $this->_minOrderTotal = $this->getConfigData('min_order_total');
		
		echo $this->_minOrderTotal; exit;
		
    }
	
	public function OLD_construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            null,
            $data
        );
 		
        $this->_code = 'omni_finance';
        $this->_minOrderTotal = $this->getConfigData('min_order_total');
    }
	
    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
		$this->_minAmount = $this->getConfigData('min_order_total');
        $this->_maxAmount = $this->getConfigData('max_order_total');
		
        if($quote && $quote->getBaseGrandTotal() < $this->_minAmount) {
            return false;
        }
		if($quote && $quote->getBaseGrandTotal() > $this->_maxAmount) {
            return false;
        }
		
        return parent::isAvailable($quote);
    }
}

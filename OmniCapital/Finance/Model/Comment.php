<?php 
namespace OmniCapital\Finance\Model;
use \Magento\Config\Model\Config\CommentInterface;
class Comment implements CommentInterface {
	
	protected $_storeManager;
	
	public function __construct(
      \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
		$this->_storeManager=$storeManager;
    }
	
    public function getCommentText($elementValue)  {
		ob_start();
		$baseUrl = $this->_storeManager->getStore()->getBaseUrl();
		?>
		<p style="background-color: #303030;color: #fff;padding: 10px;text-align: center;margin-bottom: 0;font-weight: bold;">Please copy and paste the below URLS into your Installation settings on Omniport</p>
		<table class="admin__control-table" style="border: 1px solid #ddd">
			<tbody>
				<tr>
					<th>Accepted</th>
					<td><?php echo $baseUrl; ?>/omnifinance/process/accepted</td>
				</tr>
				<tr>
					<th>Referred</th>
					<td><?php echo $baseUrl; ?>/omnifinance/process/referred</td>
				</tr>
				<tr>
					<th>Declined</th>
					<td><?php echo $baseUrl; ?>/omnifinance/process/declined</td>
				</tr>
				<tr>
					<th>Cancelled</th>
					<td><?php echo $baseUrl; ?>/omnifinance/process/cancelled</td>
				</tr>
				<tr>
					<th>Notification URL</th>
					<td><?php echo $baseUrl; ?>/omnifinance/process/</td>
				</tr>
			</tbody>
		</table>
		<p>If you want to redirect your customer to a custom page after their credit application please fill the boxes below. Leave blank for default behaviour</p>
		<p>Please enter full URL including http://<br/>E.G : http://www.domainname.com</p>
		<?php
		$content = ob_get_contents();
		ob_end_clean();
        return $content;
    }

}
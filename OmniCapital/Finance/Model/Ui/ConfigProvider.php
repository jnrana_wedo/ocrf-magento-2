<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
 
namespace OmniCapital\Finance\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Finance\Gateway\Http\Client\ClientMock;

final class ConfigProvider implements ConfigProviderInterface {
	
    const CODE = 'omni_finance';

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig() {
    	
        return [
            'payment' => [
                self::CODE => [
                    'storedCards' => $this->getStoredCards(),
                ]
            ]
        ];
    }
	
	public function getStoredCards(){
		$result = array();
		$result['0'] = "Test";
		$result['1'] = "Test1";
		return $result;
	}
	
    protected function getMinTotal() {
        return $this->config->getValue('min_order_total');
    }
}

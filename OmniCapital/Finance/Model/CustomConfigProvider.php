<?php
namespace OmniCapital\Finance\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\SamplePaymentGateway\Gateway\Http\Client\ClientMock;

/**
 * Class ConfigProvider
 */
class CustomConfigProvider implements ConfigProviderInterface {
	
    const CODE = 'omni_finance';
	protected $method;
	protected $helper;
	
	public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper, 
        \OmniCapital\Finance\Helper\Data $helper
    ) {
    	$this->helper = $helper;
        $this->method = $paymentHelper->getMethodInstance('omni_finance');
    }
	
    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'financePackages' => $this->getFinancePackages(),
                ]
            ]
        ];
    }
	
	public function getFinancePackages(){
		$result = array();
		$packages = $this->helper->getFinancePackages();
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		$cartTotal = number_format($cart->getQuote()->getGrandTotal(), 2, '.', '');
		
		foreach ($packages as $package) {
			if($cartTotal > $package['threshold']){
				$result[$package['finance_code']] = $package['package_name'];	
			}
		}
		return $result;
	}
	
}

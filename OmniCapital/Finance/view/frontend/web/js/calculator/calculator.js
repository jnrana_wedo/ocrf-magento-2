require(['jquery'],function($){
	jQuery(function () {
		var numMin = "345";
		var numMax = "26654849494";
		var adjustedHigh = (parseFloat(numMax) - parseFloat(numMin)) + 1;
		var numRand = Math.floor(Math.random() * adjustedHigh) + parseFloat(numMin);
		if ((IsNumeric(numMin) && (IsNumeric(numMax)) && (parseFloat(numMin) <= parseFloat(numMax)) && (numMin != "") && (numMax != ""))) {
			jQuery("#randomnumber").val("FINANCE" + numRand);
		}
	});
	
	jQuery(document).ready(function() {
			
		jQuery('.btnShowFinance').click(function() {
			jQuery('.financeTable').toggle();
			if (jQuery('.financeTable').is(":visible")) {
				jQuery('.btnShowFinance').html('Hide Example');
			} else {
				jQuery('.btnShowFinance').html('Show Example');
			}
		});
	}); 
	
	function IsNumeric(n) {
		return !isNaN(n);
	}
	
	jQuery( window ).on( "load", function() {
	   var jQueryprice  = jQuery("#total_price").val(),
	   jQuerypercentage = jQuery("#finance_deposit").val(),
	   jQuerydiscount   = jQuery("#goods_price").val();
	   var calcPrice  = ( jQueryprice * jQuerypercentage / 100 ).toFixed(2);
	   jQuery("#goods_price").val(calcPrice);
	
	   });
	
	function calculateFinance(baseprice,months,interest){
		var finance_deposite = jQuery(document).find(".finance_deposite").html();
		var finance_deposite_to_pay = eval(baseprice*finance_deposite)/100;
		var finance_loan_amount = baseprice-finance_deposite_to_pay;
		var finance_interest = jQuery(document).find(".finance_interest").val();
		var finance_loan_repayment = eval(finance_loan_amount + (eval(finance_loan_amount*finance_interest)/100));
		var finance_months = jQuery(document).find(".finance_months").val();
		var finance_cost_of_loan = finance_loan_repayment -  finance_loan_amount;
		var finance_total_amount_payable = eval(baseprice) + finance_cost_of_loan;
		var finance_monthly_payment = finance_loan_repayment / finance_months;
	
		jQuery('.hideIfZeroROI').show();
		if(finance_interest == 0){
			jQuery('.hideIfZeroROI').hide();
		}
	
		jQuery(document).find(".finance_deposite_to_pay").html(finance_deposite_to_pay.toFixed(2));
		jQuery(document).find(".finance_loan_amount").html(finance_loan_amount.toFixed(2));
		jQuery(document).find(".finance_loan_repayment").html(finance_loan_repayment.toFixed(2));
		jQuery(document).find(".finance_number_of_months").html(finance_months);
		jQuery(document).find(".finance_cost_of_loan").html(finance_cost_of_loan.toFixed(2));
		jQuery(document).find(".finance_total_amount_payable").html(finance_total_amount_payable.toFixed(2));
		jQuery(document).find(".finance_monthly_payment").html(finance_monthly_payment.toFixed(2));
		jQuery(document).find(".finance_roi").html(finance_interest);
	}
	
	function getMinMonthlyVal(baseprice,months,interest) {
		var finance_deposite = jQuery(document).find(".finance_deposite").html();
		var finance_deposite_to_pay = eval(baseprice*finance_deposite)/100;
		var finance_loan_amount = baseprice-finance_deposite_to_pay;
		var finance_interest = interest;
		var finance_loan_repayment = eval(finance_loan_amount + (eval(finance_loan_amount*finance_interest)/100));
		var finance_months = months;
		var finance_cost_of_loan = finance_loan_repayment -  finance_loan_amount;
		var finance_total_amount_payable = eval(baseprice) + finance_cost_of_loan;
		var finance_monthly_payment = finance_loan_repayment / finance_months;
	    return finance_monthly_payment; 
	}
	
	jQuery(document).ready(function(){
	    var basepricearray = [];
	    var finance_deposite = jQuery(document).find(".finance_deposite").html();
	    var current_month = jQuery(document).find( ".finance_options option:selected" ).attr("months");
		var current_interest = jQuery(document).find( ".finance_options option:selected" ).attr("interest");
		jQuery(document).find(".finance_interest").val(current_interest);
		jQuery(document).find(".finance_months").val(current_month);
	    var baseprice = jQuery(document).find("span.finance_cash_price").html();	
		calculateFinance(baseprice,current_month,current_interest);
		jQuery(document).on("change",".finance_options",function(){
			var current_month = jQuery(document).find( ".finance_options option:selected" ).attr("months");
			var current_interest = jQuery(document).find( ".finance_options option:selected" ).attr("interest");
			jQuery(document).find(".finance_interest").val(current_interest);
			jQuery(document).find(".finance_months").val(current_month);
		    baseprice = jQuery(document).find("span.finance_cash_price").html();
			calculateFinance(baseprice,current_month,current_interest);
			var finance_deposite = jQuery(document).find(".finance_deposite").html(); 
			var finance_deposite_to_pay = eval(baseprice*finance_deposite)/100;
			jQuery(document).find(".finance_deposite_to_pay").html(finance_deposite_to_pay.toFixed(2));
		});
		var $content = jQuery(document).find(".fin_hidden").hide();
		jQuery(document).find(".show-more").html("<i class='arrow down'></i>&nbsp; See Example");
		jQuery(".show-more").on("click", function(e) {
			if (jQuery(".cartform table").is(":visible")) {
				jQuery(this).html("<i class='arrow down'></i>&nbsp; See Example");
			} else {
				jQuery(this).html("<i class='arrow up'></i>&nbsp; See Example");
			}
			$content.slideToggle();
		}); 
	
	    var maxmonth = 0;
	    var maxinterest = 0;
	    jQuery(".finance_options option").each(function(){
		    var thismonth= jQuery(this).attr("months");
		    var thisinterest= jQuery(this).attr("interest");
		    if(thismonth > maxmonth){  
				maxmonth = eval(thismonth);
		        maxinterest = eval(thisinterest);
		    }
	    });
	
	    if(jQuery(".variant_price_select").length > 0){
	        jQuery(".variant_price_select option").each(function(){
	        var thisprice = jQuery(this).attr("value");
	        var minvalue = getMinMonthlyVal(thisprice,maxmonth,maxinterest);
	        basepricearray.push(minvalue);
	        });
	    }else{
	        var thisprice = jQuery("span.finance_cash_price").html();
	        var minvalue = getMinMonthlyVal(thisprice,maxmonth,maxinterest);
	        basepricearray.push(minvalue); 
	    }
	    jQuery(".min_finance_monthly_payment").html(Math.min.apply(Math,basepricearray).toFixed(2));
	    jQuery(document).find('#p_method_omni_finance').click(function(){ 
			var basepricearray = []; 
		    var current_month = jQuery(document).find( ".finance_options option:selected" ).attr("months");  
			var current_interest = jQuery(document).find( ".finance_options option:selected" ).attr("interest");
			jQuery(document).find(".finance_interest").val(current_interest);
			jQuery(document).find(".finance_months").val(current_month);
		    var baseprice = jQuery(document).find("span.finance_cash_price").html();	
			calculateFinance(baseprice,current_month,current_interest); 
		});
	
		jQuery(document).find('#shipping-method-buttons-container button').click(function(){ 
			setTimeout(function(){  
				var basepricearray = []; 
			    var current_month = jQuery(document).find( ".finance_options option:selected" ).attr("months");  
				var current_interest = jQuery(document).find( ".finance_options option:selected" ).attr("interest");
				jQuery(document).find(".finance_interest").val(current_interest);
				jQuery(document).find(".finance_months").val(current_month);
			    var baseprice = jQuery(document).find("span.finance_cash_price").html();	
				calculateFinance(baseprice,current_month,current_interest);	
			}, 2000);
		});
	});
});
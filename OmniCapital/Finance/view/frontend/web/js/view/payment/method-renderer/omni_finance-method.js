define(
    [
        'jquery',
	    'Magento_Checkout/js/view/payment/default',
	    'Magento_Checkout/js/action/place-order',
	    'Magento_Checkout/js/action/select-payment-method',
	    'Magento_Customer/js/model/customer',
	    'Magento_Checkout/js/checkout-data',
	    'Magento_Checkout/js/model/payment/additional-validators',
	    'mage/url',
    ],
    function (
    	$,
    	Component,
    	placeOrderAction,
	    selectPaymentMethodAction,
	    customer,
	    checkoutData,
	    additionalValidators,
	    url
    ) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'OmniCapital_Finance/payment/omni_finance', 
                financePackage: ''
            },
            
            placeOrder: function (data, event) {
            	if (event) {
                    event.preventDefault();
                }
                var self = this,
                    placeOrder,
                    emailValidationResult = customer.isLoggedIn(),
                    loginFormSelector = 'form[data-role=email-with-possible-login]';
                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }
                if (emailValidationResult && this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                    placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                    $.when(placeOrder).fail(function () {
                        self.isPlaceOrderActionAllowed(true);
                    }).done(this.afterPlaceOrder.bind(this));
                    return true;
                }
                return false;
            },
            
            initObservable: function () {
                this._super()
                    .observe([
                        'financePackage'
                    ]);
                return this;
            },
            
            selectPaymentMethod: function() {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod(this.item.method);
                return true;
            },
            
            afterPlaceOrder: function () {
                window.location.replace(url.build('omnifinance/redirect/index/'));
            },
            
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            
            getFinancePackages: function() {
	            return window.checkoutConfig.payment.omni_finance.financePackages;
	        },
            
            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'finance_package': this.financePackage() 
                    }
                };
            },
            
            getFinancePackageList: function() {
	            return _.map(this.getFinancePackages(), function(value, key) {
	                return {
	                    'value': key,
	                    'type': value
	                }
	            });
	        },
            
        });
    }
);

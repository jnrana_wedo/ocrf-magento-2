define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'omni_finance',
                component: 'OmniCapital_Finance/js/view/payment/method-renderer/omni_finance-method'
            }
        );
        return Component.extend({});
    }
);
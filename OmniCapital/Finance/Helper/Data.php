<?php
namespace OmniCapital\Finance\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;
 
class Data extends AbstractHelper {
    
    protected $_scopeConfig;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
        $this->_scopeConfig = $scopeConfig;
    }

	/*
    public function RandomFunc(){
        return $this->_scopeConfig->getValue('payment/omni_finance/title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
    }*/
	
	public function getAcceptUrl($value='') {
		return $this->_scopeConfig->getValue('payment/omni_finance/accepted_redirect_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getRefferedUrl($value='') {
		return $this->_scopeConfig->getValue('payment/omni_finance/reffered_redirect_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getDeclinedUrl($value='') {
		return $this->_scopeConfig->getValue('payment/omni_finance/declined_redirect_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getCancelledUrl($value='') {
		return $this->_scopeConfig->getValue('payment/omni_finance/cancelled_redirect_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getApiKey() {
		return $this->_scopeConfig->getValue('payment/omni_finance/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getRetailerUniqueRef($length = 22, $prefix = 'MAGENTOCE2') {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
		$length = $length - strlen($prefix);
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $prefix.$randomString;
	}
	
	public function getInstallationId() {
		return $this->_scopeConfig->getValue('payment/omni_finance/installation_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
    public function getFinancePackages(){
        $packages = $this->_scopeConfig->getValue('payment/omni_finance/finance_packages', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		
		if(isset($packages) && $packages != ''){
			$packages = unserialize($packages);
			if(is_array($packages) && count($packages) > 0){
				foreach ($packages as $key => $package) {
					$fcode = $package['finance_code'];
					$string_interest = substr($package['finance_code'], strpos($package['finance_code'], "-") + 1);
					$string_month = substr($package['finance_code'].'-', 0, strpos($package['finance_code'], '-'));
					$length_month = substr($string_month, 4);
					$interest = substr($string_interest.'-', 0, strpos($string_interest, '-'));
					$packages[$key]['months'] = $length_month;
					$packages[$key]['interest'] = $interest;
				}
				return $packages;
			}
		}
        return FALSE;
    }

    public function getDepositpPercentage(){
        return $this->_scopeConfig->getValue('payment/omni_finance/deposit_percentage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCurrentCurrencyCode(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $currencysymbol = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
        $currency = $currencysymbol->getStore()->getCurrentCurrencyCode();
        $currency = $objectManager->create('Magento\Directory\Model\CurrencyFactory')->create()->load($currency);
        $currencySymbol = $currency->getCurrencySymbol();
        return $currencySymbol;
    }

    public function getMinOrderPrice(){
        return $this->_scopeConfig->getValue('payment/omni_finance/min_order_total', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getMaxOrderPrice(){
        return $this->_scopeConfig->getValue('payment/omni_finance/max_order_total', \Magento\Store\Model\ScopeInterface::SCOPE_STORE); 
    }

}
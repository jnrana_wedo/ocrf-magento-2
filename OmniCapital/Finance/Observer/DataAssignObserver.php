<?php

namespace OmniCapital\Finance\Observer;

use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;

class DataAssignObserver extends AbstractDataAssignObserver {
	
    const PAYMENT_METHOD_NONCE = 'omni_finance';

    protected $additionalInformationList = [
        self::PAYMENT_METHOD_NONCE,
    ];
    
    public function execute(Observer $observer) {
        $method = $this->readMethodArgument($observer);
        $data = $this->readDataArgument($observer);
		$data = $data->getData(); 
		$financePackage = $data['additional_data']['finance_package'];
        $paymentInfo = $method->getInfoInstance();
        if ($financePackage && $financePackage !== null) {
            $paymentInfo->setAdditionalInformation(
                'finance_package',
                $financePackage
            );
        }
    }
}

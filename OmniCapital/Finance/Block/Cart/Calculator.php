<?php
namespace OmniCapital\Finance\Block\Cart;

class Calculator extends \Magento\Framework\View\Element\Template {
	
	protected $helper;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\OmniCapital\Finance\Helper\Data $helper,
        array $data = []
    ) {
		$this->helper = $helper;
        parent::__construct($context, $data);
    }
	
	public function getFinancePackages(){
		return $this->helper->getFinancePackages();
	}
	
	public function getDepositpPercentage(){
		return $this->helper->getDepositpPercentage();
	}

	public function getCurrencyCode(){
		return $this->helper->getCurrentCurrencyCode();
	} 

	public function getBasePrice() {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
		return number_format($cart->getQuote()->getGrandTotal(), 2, '.', '');
	}
	
	public function getMinOrderPrice(){
		return $this->helper->getMinOrderPrice();
	}

	public function getMaxOrderPrice(){
		return $this->helper->getMaxOrderPrice();
	}

}
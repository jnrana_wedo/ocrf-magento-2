<?php
namespace OmniCapital\Finance\Block\Product\View;
use Magento\Catalog\Block\Product\AbstractProduct;

class Calculator extends AbstractProduct {
	
	protected $configProvider;
	protected $helper;
	
	public function __construct(
		\Magento\Catalog\Block\Product\Context $context, 
		\OmniCapital\Finance\Helper\Data $helper,
		array $data = []
	) {
		$this->helper = $helper;
	    parent::__construct($context, $data);
    }
	
	public function getFinancePackages(){
		return $this->helper->getFinancePackages();
	}
	
	public function getDepositpPercentage(){
		return $this->helper->getDepositpPercentage();
	}

	public function getCurrencyCode(){
		return $this->helper->getCurrentCurrencyCode();
	} 

	public function getBasePrice() {
		return number_format($this->getProduct()->getFinalPrice(), 2, '.', '');
	}
	
	public function getMinOrderPrice(){
		return $this->helper->getMinOrderPrice();
	}

	public function getMaxOrderPrice(){
		return $this->helper->getMaxOrderPrice();
	}

}
<?php

namespace OmniCapital\Finance\Block\Redirect;
use Magento\Payment\Helper\Data as PaymentHelper;

class Index extends \Magento\Framework\View\Element\Template {
	
	protected $_checkoutSession;
    protected $_orderFactory;
    protected $_scopeConfig;
	protected $_paymentHelper;
	
	protected $_liveUrl	= 'https://omniport.omnicapital.co.uk/credit_app/Default.aspx';
    protected $_testUrl = 'https://omniporttest.ocrf.co.uk/credit_app/Default.aspx';
	
    public function __construct(
    	\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory, 
        PaymentHelper $paymentHelper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        $this->_scopeConfig = $context->getScopeConfig();
		$this->_paymentHelper = $paymentHelper;
		$this->setProcessing();
		parent::__construct($context);
    }
	
	public function setProcessing($value='') {
		$order = $this->getOrder();
		$order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
	    $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
	    $order->addStatusToHistory($order->getStatus(), 'User Redirected to Omni Finance.');
	    $order->save();
	}
	
    public function getRealOrderId() {
        $lastorderId = $this->_checkoutSession->getLastOrderId();
        return $lastorderId;
    }

    public function getOrder() {
        if ($this->_checkoutSession->getLastRealOrderId()) {
             $order = $this->_orderFactory->create()->loadByIncrementId($this->_checkoutSession->getLastRealOrderId());
        	return $order;
        }
        return false;
    }
	
	public function getApiKey() {
		return $this->_scopeConfig->getValue('payment/omni_finance/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getRetailerUniqueRef($length = 22, $prefix = 'MAGENTOCE2') {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
		$length = $length - strlen($prefix);
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $prefix.$randomString;
	}
	
	public function getInstallationId() {
		return $this->_scopeConfig->getValue('payment/omni_finance/installation_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getDepositPercentage() {
		return $this->_scopeConfig->getValue('payment/omni_finance/deposit_percentage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
	
	public function getCartItemNames() {
		$order = $this->getOrder();
    	$items = $order->getAllItems();
		$names = array();
		foreach($items as $item) {
	        $product = $item->getProduct();
	        $names[] = $product->getName();
	    }
		return implode(',', $names);
	}
	
	public function getFinanceCode() {
		$order = $this->getOrder();
		$payment = $order->getPayment();
		$data = $payment->getData();
		$financePackage = $data['additional_information']['finance_package'];
		return $financePackage;
	}
	
	public function getCartTotal() {
		return $this->getOrder()->getGrandTotal();
	}
	
    public function getShippingInfo() {
        $order = $this->getOrder();
        if($order) {
            $address = $order->getShippingAddress();    
            return $address;
        }
        return false;
    }
	
	public function getFormAction() {
		$mode = $this->_scopeConfig->getValue('payment/omni_finance/test_mode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		if($mode == 1){
			return $this->_testUrl;
		}
		return $this->_liveUrl;
	}
	
}

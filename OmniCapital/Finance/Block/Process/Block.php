<?php
namespace OmniCapital\Finance\Block\Process;

use Magento\Payment\Helper\Data as PaymentHelper;

class Block extends \Magento\Framework\View\Element\Template {
	
	protected $_checkoutSession;
    protected $_orderFactory;
    protected $_scopeConfig;
	protected $_paymentHelper;
	protected $_request; 
	protected $_response;
	
	protected $_liveUrl	= 'https://omniport.omnicapital.co.uk/credit_app/Default.aspx';
    protected $_testUrl = 'https://omniporttest.ocrf.co.uk/credit_app/Default.aspx';
	
    public function __construct(
    	\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory, 
        PaymentHelper $paymentHelper,
        \Magento\Framework\App\Request\Http $request, 
        \Magento\Framework\App\Response\Http $response
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        $this->_scopeConfig = $context->getScopeConfig();
		$this->_paymentHelper = $paymentHelper;
		$this->_request = $request;
		$this->_response = $response;
		
		$action = $this->_request->getActionName();
		$this->processOrder($action);
		
		parent::__construct($context);
    }
	
	public function processOrder($action) {
		$order = $this->getOrder();
		
		switch ($action) {
			case 'accepted':
				$order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
			    $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
			    $order->addStatusToHistory($order->getStatus(), 'Finance was Approved.');
			    $order->save();
				break;
			
			case 'referred':
				$order->setState(\Magento\Sales\Model\Order::STATE_HOLDED, true);
			    $order->setStatus(\Magento\Sales\Model\Order::STATE_HOLDED);
			    $order->addStatusToHistory($order->getStatus(), 'A decision could not be made at this time.');
			    $order->save();
				break;
				
			case 'cancelled':
				$order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true);
			    $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
			    $order->addStatusToHistory($order->getStatus(), 'The Finance was Cancelled.');
			    $order->save();
				break;
				
			case 'declined':
				$order->setState(\Magento\Sales\Model\Order::STATE_CANCELED, true);
			    $order->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
			    $order->addStatusToHistory($order->getStatus(), 'Finance was Rejected.');
			    $order->save();
				break;
			
			default:
				
				break;
		}
	}
	
	public function getRealOrderId() {
        $lastorderId = $this->_checkoutSession->getLastOrderId();
        return $lastorderId;
    }

    public function getOrder() {
        if ($this->_checkoutSession->getLastRealOrderId()) {
             $order = $this->_orderFactory->create()->loadByIncrementId($this->_checkoutSession->getLastRealOrderId());
        	return $order;
        }
        return false;
    }
	
}
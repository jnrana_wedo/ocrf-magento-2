<?php

namespace OmniCapital\Finance\Block;
use Magento\Payment\Helper\Data as PaymentHelper;

class Success  extends \Magento\Framework\View\Element\Template {
	
	protected $_checkoutSession;
    protected $_orderFactory;
    protected $_scopeConfig;
	protected $_paymentHelper;
	
	public function __construct(
    	\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory, 
        PaymentHelper $paymentHelper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        $this->_scopeConfig = $context->getScopeConfig();
		$this->_paymentHelper = $paymentHelper;
		parent::__construct($context);
    }
	
	public function isOmniFinanceOrder() {
		$order = $this->getOrder();
		$payment = $order->getPayment();
		$method = $payment->getMethodInstance();
		$methodCode = $method->getCode();
		if($methodCode == 'omni_finance'){
			return TRUE;
		}
		return FALSE;
	}
	
	public function getRealOrderId() {
        $lastorderId = $this->_checkoutSession->getLastOrderId();
        return $lastorderId;
    }

    public function getOrder() {
        if ($this->_checkoutSession->getLastRealOrderId()) {
             $order = $this->_orderFactory->create()->loadByIncrementId($this->_checkoutSession->getLastRealOrderId());
        	return $order;
        }
        return false;
    }

}
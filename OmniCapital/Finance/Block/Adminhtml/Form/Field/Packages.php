<?php
namespace OmniCapital\Finance\Block\Adminhtml\Form\Field;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class Packages extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray {
    protected $_activation;
    protected function _prepareToRender() {
		$this->addColumn('package_name', ['label' => __('Finance Package Name')]);
        $this->addColumn('finance_code', ['label' => __('Finance Code'), 'class' => 'omniFinanceCode',]);
		$this->addColumn('threshold', ['label' => __('Threshold')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }
}
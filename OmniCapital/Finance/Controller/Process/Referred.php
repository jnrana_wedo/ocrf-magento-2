<?php
namespace OmniCapital\Finance\Controller\Process;

use Magento\Framework\Controller\ResultFactory; 

class Referred extends \Magento\Framework\App\Action\Action {
	
	protected $helper;
    protected $resultPageFactory;
	
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \OmniCapital\Finance\Helper\Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->helper = $helper;
        parent::__construct($context);
    }

    public function execute() {
    	$this->messageManager->addWarning(__("A decision could not be made at this time."));
		
		$redirectUrl = $this->helper->getRefferedUrl();
		if($redirectUrl && $redirectUrl != ''){
			ob_start();
		    header('Location: '.$redirectUrl);
		    ob_end_flush();
		    die();
		}
		
    	$this->_redirect('checkout/cart');
        return $this->resultPageFactory->create();
    }
}

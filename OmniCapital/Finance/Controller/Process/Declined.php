<?php
namespace OmniCapital\Finance\Controller\Process;

use Magento\Framework\Controller\ResultFactory; 

class Declined extends \Magento\Framework\App\Action\Action {
	
	protected $helper;
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \OmniCapital\Finance\Helper\Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->helper = $helper;
        parent::__construct($context);
    }

    public function execute() {
    	$this->messageManager->addWarning(__("Sorry finance was declined and your order could not be completed. Please re-order and choose a different payment method. "));
		
		$redirectUrl = $this->helper->getDeclinedUrl();
		if($redirectUrl && $redirectUrl != ''){
			ob_start();
		    header('Location: '.$redirectUrl);
		    ob_end_flush();
		    die();
		}
		
    	$this->_redirect('checkout/cart');
        return $this->resultPageFactory->create();
    }
}

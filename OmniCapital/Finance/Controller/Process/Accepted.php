<?php
namespace OmniCapital\Finance\Controller\Process;

use Magento\Framework\Controller\ResultFactory; 

class Accepted extends \Magento\Framework\App\Action\Action {
	
	protected $helper; 
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory, 
        \OmniCapital\Finance\Helper\Data $helper
    ) {
		$this->resultPageFactory = $resultPageFactory;
		$this->helper = $helper;
		parent::__construct($context);
    }

    public function execute() {
		$redirectUrl = $this->helper->getAcceptUrl();
		if($redirectUrl && $redirectUrl != ''){
			ob_start();
		    header('Location: '.$redirectUrl);
		    ob_end_flush();
		    die();
		}
    	$this->_redirect('checkout/onepage/success');
        return $this->resultPageFactory->create();
    }
}
